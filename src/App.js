import './App.css';
import { Routes, Route } from 'react-router-dom';
import DefaultLayout from "./layouts/DefaultLayout";
import NameTest from "./components/NameTest";
import Stylist from "./components/Stylist";
import RandomBox from "./components/RandomBox";
import DutchPay from "./components/DutchPay";

function App() {
  return (
    <div className="App">
      <Routes>
          <Route path={"/move"} element={ <DefaultLayout><NameTest/></DefaultLayout> }/>
          <Route path={"/style"} element={ <DefaultLayout><Stylist/></DefaultLayout> }/>
          <Route path={"/random"} element={ <DefaultLayout><RandomBox/></DefaultLayout> }/>
          <Route path={"/pay"} element={ <DefaultLayout><DutchPay/></DefaultLayout> }/>
      </Routes>
    </div>
  );
}

export default App;
