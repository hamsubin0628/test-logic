import React, { useState } from 'react';

const RandomBox = () => {
    const INIT_PRICE = 50000
    const BOX_PRICE = 5000

    const [cash, setCash] = useState(INIT_PRICE)
    const [history, setHistory] = useState([])

    const goods = [
        { name: "자동차", percent: 0.01 },
        { name: "맥북", percent: 0.09 },
        { name: "여행용티슈", percent: 0.5 },
        { name: "츄파춥스", percent: 0.4 },
    ]

    // 소세지 나누기 기법
    const getRandomGoods = () => {
        const rand = Math.random()
        let sum = 0
        for (const good of goods) {
            sum += good.percent
            if (rand < sum) return good
        }
    }

    const openBox = () => {
        if (cash < BOX_PRICE) return false
        const good = getRandomGoods()
        setCash(cash - BOX_PRICE)
        setHistory([...history, good.name])
    }

    const resetGame = () => {
        setCash(INIT_PRICE)
        setHistory([])
    }

    return (
        <div>
            <h1>랜덤박스깡</h1>
            <p>보유 현금 : {cash}원</p>
            <button onClick={openBox}>박스열기</button>
            <button onClick={resetGame}>다시하기</button>
            <h5>당첨기록</h5>
            {history.map((item, index) => (
                <p key={index}>{item}</p>
                ))}
        </div>
    )
}

export default RandomBox;