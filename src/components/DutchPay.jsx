import React, { useState } from 'react';

const DutchPay = () => {
    const [pay, setPay] = useState('');
    const [userName1, setUserName1] = useState('');
    const [userName2, setUserName2] = useState('');
    const [userName3, setUserName3] = useState('');
    const [payResult, setPayResult] = useState('');

    const handlePay = e => {
        setPay(e.target.value);
    }

    const handleUserName1 = e => {
        setUserName1(e.target.value);
    }

    const handleUserName2 = e => {
        setUserName2(e.target.value);
    }

    const handleUserName3 = e => {
        setUserName3(e.target.value);
    }

    const calculatePay = () => {
        const percent1 = Math.floor((Math.random() * pay))
        const percent2 = Math.floor((Math.random() * pay - percent1))
        const percent3 = Math.floor((Math.random() * pay - percent2))
        setPayResult()
    }

    return (
        <div>
            <h1>더치패이를 해보자</h1>
            <div>금액 : <input type="number" value={pay} onChange={handlePay}/> 원</div>
            <h5>더치패이 할 사람</h5>
            <div>친구 1 : <input type="text" value={userName1} onChange={handleUserName1}/></div>
            <div>친구 2 : <input type="text" value={userName2} onChange={handleUserName2}/></div>
            <div>친구 3 : <input type="text" value={userName3} onChange={handleUserName3}/></div>
            <br/>
            <button onClick={calculatePay}>복불복 n빵 하기</button>
            <h5>복불복 결과</h5>
            <p>{userName1} : {payResult}원</p>
            <p>{userName2} : {payResult}원</p>
            <p>{userName3} : {payResult}원</p>
            <p>지불하세요~</p>
        </div>
    )
}

export default DutchPay;