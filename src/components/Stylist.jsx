import React, { useState } from 'react';

const Stylist = () => {
    const [tShirt, setTShirt] = useState('');
    const [pants, setPants] = useState('');
    const [tPick, setTPick] = useState('');
    const [pantsPick, setPantsPick] = useState('');

    const handleTShirt = e => {
        setTShirt(e.target.value);
    }

    const handlePants = e => {
        setPants(e.target.value);
    }

    const calculatePickClothes = () => {
        const tArr = tShirt.split(',')
        const pantsArr = pants.split(',')
        setTPick(tArr[Math.floor(Math.random() * tArr.length)])
        setPantsPick(pantsArr[Math.floor(Math.random() * pantsArr.length)])
    }

    return(
        <div>
            <h1>내일 머입지?</h1>
            <div>상의 : <input type="text" value={tShirt} onChange={handleTShirt}/></div>
            <div>하의 : <input type="text" value={pants} onChange={handlePants}/></div>
            <br/>
            <button onClick={calculatePickClothes}>내일 머입지</button>

            <p>상의 : {tPick}</p>
            <p>하의 : {pantsPick}</p>
            <p>이렇게 입으세요</p>
        </div>
    )
}

export default Stylist;