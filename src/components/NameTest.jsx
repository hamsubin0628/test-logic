import React,{ useState } from 'react';

const NameTest = () => {
    const [name1, setName1] = useState('')
    const [name2, setName2] = useState('')
    const [score, setScore] = useState(0)

    const handleName1 = e => {
        setName1(e.target.value)
    }

    const handleName2 = e => {
        setName2(e.target.value)
    }

    const calculateScore = () => {
        setScore(Math.floor(Math.random() * 101))
    }

    return (
        <div>
            <h1>이름 궁합 보기</h1>
            <input type="text" value={name1} onChange={handleName1}/>
            <input type="text" value={name2} onChange={handleName2}/>
            <button onClick={calculateScore}>궁합 보기</button>
            <p>{name1}와 {name2}의 궁합도 : {score}%</p>
        </div>
    )
}

export default NameTest;