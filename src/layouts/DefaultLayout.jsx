import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
        <div>헤더</div>
            <div className="hsb-menu">
                <Link to="/move"><nav className="hsb-menu-name">궁합도 보기</nav></Link>
                <Link to="/style"><nav className="hsb-menu-name">코디네이터</nav></Link>
                <Link to="/random"><nav className="hsb-menu-name">랜덤박스</nav></Link>
                <Link to="/pay"><nav className="hsb-menu-name">더치패이</nav></Link>
            </div>
            <main>{children}</main>
            <footer>푸터</footer>
        </>
    )
}

export default DefaultLayout;